package com.experis.no.repositories;

import com.experis.no.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CharacterRepository extends JpaRepository<MovieCharacter, Long> {
}
