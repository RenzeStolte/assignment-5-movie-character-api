package com.experis.no.repositories;

import com.experis.no.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
