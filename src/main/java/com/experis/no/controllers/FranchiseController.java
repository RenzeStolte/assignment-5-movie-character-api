package com.experis.no.controllers;

import com.experis.no.models.Franchise;
import com.experis.no.models.Movie;
import com.experis.no.models.MovieCharacter;
import com.experis.no.repositories.FranchiseRepository;
import com.experis.no.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    /**
     * Get all franchises
     *
     * @return ResponseEntity with the franchise and HTTPStatus
     */
    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "204", description = "GET request successful, but no franchises in database",
                    content = @Content)
    })
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus respond;
        List<Franchise> franchises = franchiseRepository.findAll();

        if (franchises.size() == 0) {
            respond = HttpStatus.NO_CONTENT;
        } else {
            respond = HttpStatus.OK;
        }
        return new ResponseEntity<>(franchises, respond);
    }

    /**
     * add new franchise
     *
     * @param franchise the new franchise
     * @return ResponseEntity containing
     */
    @Operation(summary = "Add a new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Franchise added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))})
    })
    @PostMapping()
    public ResponseEntity<Franchise> addNewFranchise(@RequestBody() Franchise franchise) {
        HttpStatus status = HttpStatus.CREATED;
        Franchise retFranchise = franchiseRepository.save(franchise);
        return new ResponseEntity<>(retFranchise, status);
    }

    /**
     * Get the movie with the provided id.
     *
     * @param id ID of the movie.
     * @return ResponseEntity with the movie and a HTTPStatus.
     */
    @Operation(summary = "Get a franchise by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    public ResponseEntity<Franchise> getFranchise(
            @Parameter(description = "id of franchise to get")
            @PathVariable(value = "id") Long id) {

        HttpStatus respond;
        Franchise franchise = new Franchise();

        if (!franchiseRepository.existsById(id)) {
            respond = HttpStatus.NOT_FOUND;
        } else {
            franchise = franchiseRepository.getOne(id);
            respond = HttpStatus.OK;
        }
        return new ResponseEntity<>(franchise, respond);
    }

    /**
     * Removes franchise from database
     *
     * @param id id of the franchise
     * @return ResponseEntity with http status
     */
    @Operation(summary = "Delete a franchise by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deletion successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Franchise> deleteFranchise(
            @Parameter(description = "id of franchise to delete")
            @PathVariable() Long id) {
        HttpStatus status;

        if (!franchiseRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        
        franchiseRepository.delete(franchiseRepository.getOne(id));
        status = HttpStatus.OK;

        return new ResponseEntity<>(status);
    }

    /**
     * Update existing franchise
     *
     * @param id        franchise id
     * @param franchise The new franchise
     * @return ResponseEntity with http status
     */
    @Operation(summary = "Update a franchise by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content)})
    @PutMapping(value = "/{id}")
    public ResponseEntity<Franchise> updateFranchise(
            @Parameter(description = "id of franchise to update")
            @PathVariable() Long id,
            @RequestBody() Franchise franchise) {

        HttpStatus status;
        Franchise retFranchise = new Franchise();

        if (!franchiseRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;

        }else{
            retFranchise = franchiseRepository.save(franchise);
            status = HttpStatus.OK;
        }

        return new ResponseEntity<>(retFranchise, status);
    }


    /**
     * Fetch all movies in a franchise
     *
     * @param id Franchise id
     * @return ResponseEntity -> with  Set<movies> and http Status
     */
    @Operation(summary = "Get movies in franchise by franchise's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content)})
    @GetMapping(value = "/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesByFranchise(
            @Parameter(description = "id of franchise to get movies from")
            @PathVariable(value = "id") Long id) {

        // Check if franchise exists
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(new HashSet<>(), HttpStatus.NOT_FOUND);
        }

        Franchise franchise = franchiseRepository.getOne(id);
        HttpStatus respond = HttpStatus.OK;

        Set<Movie> movies = franchise.getMovies();
        return new ResponseEntity<>(movies, respond);
    }

    /**
     * Fetch all characters in a franchise
     *
     * @param id Franchise id
     * @return ResponseEntity -> with  Set<characters> and http Status
     */
    @Operation(summary = "Get characters in franchise by franchise's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content)})
    @GetMapping(value = "/{id}/characters")
    public ResponseEntity<Set<MovieCharacter>> getCharactersByFranchise(
            @Parameter(description = "id of franchise to get characters from")
            @PathVariable(value = "id") Long id) {

        // Check if franchise exists
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(new HashSet<>(), HttpStatus.NOT_FOUND);
        }

        Franchise franchise = franchiseRepository.getOne(id);
        HttpStatus respond = HttpStatus.OK;

        Set<MovieCharacter> chars = new HashSet<>();

        for (Movie movie : franchise.getMovies()) {
            chars.addAll(movie.getCharacters());
        }
        return new ResponseEntity<>(chars, respond);
    }


}
