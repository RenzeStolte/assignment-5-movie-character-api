package com.experis.no.controllers;

import com.experis.no.models.Movie;
import com.experis.no.models.MovieCharacter;
import com.experis.no.repositories.CharacterRepository;
import com.experis.no.repositories.FranchiseRepository;
import com.experis.no.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Get all the movies in the database.
     *
     * @return List with all the movies.
     */
    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))})})
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        HttpStatus respond = HttpStatus.OK;
        return new ResponseEntity<>(movies, respond);
    }

    /**
     * Adds a new movie to the database.
     *
     * @param movie Movie that is to be added to the database.
     * @return ResponseEntity with the created Movie and a HTTPStatus
     */
    @Operation(summary = "Add a new movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Movie added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid information given",
                    content = @Content)})
    @PostMapping()
    public ResponseEntity<Movie> addNewMovie(@RequestBody() Movie movie) {
        // Check that all characters are valid, if any are provided. If any are invalid, return BAD_REQUEST.
        Set<MovieCharacter> characters = movie.getCharacters();
        if (characters != null && !charactersExist(characters))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        // Check that the franchise ID exists in the database, if there is one.
        // If not, return a BAD_REQUEST.
        if (movie.getFranchise() != null && !franchiseRepository.existsById(movie.getFranchise().getId())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        // Save the movie and OK the response.
        Movie respondMovie = movieRepository.save(movie);
        return new ResponseEntity<>(respondMovie, HttpStatus.CREATED);
    }

    /**
     * Updates a movie in the database.
     *
     * @param id    ID of the movie to be updated.
     * @param movie Movie containing the information that is to be updated.
     * @return Movie with updated information.
     */
    @Operation(summary = "Update a movie by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id given",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content)})
    @PutMapping(value = "/{id}")
    public ResponseEntity<Movie> updateMovie(
            @Parameter(description = "id of movie to update")
            @PathVariable(value = "id") Long id,
            @RequestBody() Movie movie) {
        // If mismatch between id's, return a BAD_REQUEST
        if (movie.getId() != id)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        // If the movie, one of the characters or franchise doesn't exist, return a bad request.
        if (!movieRepository.existsById(id) ||
                (movie.getCharacters() != null && !charactersExist(movie.getCharacters())) ||
                (movie.getFranchise() != null && !franchiseRepository.existsById(movie.getFranchise().getId()))) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        movieRepository.save(movie);
        return new ResponseEntity<>(movieRepository.getOne(movie.getId()), HttpStatus.OK);
    }

    /**
     * Get the movie with the provided id.
     *
     * @param id ID of the movie.
     * @return ResponseEntity with the movie and a HTTPStatus.
     */
    @Operation(summary = "Get a movie by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    public ResponseEntity<Movie> getMovie(
            @Parameter(description = "id of movie to get")
            @PathVariable(value = "id") Long id) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Movie movie = movieRepository.getOne(id);
        HttpStatus respond = HttpStatus.OK;
        return new ResponseEntity<>(movie, respond);
    }

    /**
     * Removes the movie with the provided id from the database.
     *
     * @param id Id of movie to remove.
     * @return A ResponseEntity containing the ID of the removed movie and a HTTPStatus.
     */
    @Operation(summary = "Delete a movie by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deletion successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> removeMovie(
            @Parameter(description = "id of movie to delete")
            @PathVariable(value = "id") Long id) {
        // Check that the ID exists, otherwise return a NOT_FOUND.
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        movieRepository.delete(movieRepository.getOne(id));
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    /**
     * Gets characters for a specific movie
     *
     * @param id Id of movie
     * @return A ResponseEntity containing characters from the movie specified and a HTTPStatus
     */
    @Operation(summary = "Get all characters in a movie by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content)})
    @GetMapping(value = "/{id}/characters")
    public ResponseEntity<Set<MovieCharacter>> getCharactersByMovie(
            @Parameter(description = "id of movie to get characters from")
            @PathVariable(value = "id") Long id) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Movie movie = movieRepository.getOne(id);
        HttpStatus respond = HttpStatus.OK;

        Set<MovieCharacter> chars = movie.getCharacters();
        return new ResponseEntity<>(chars, respond);
    }

    /**
     * Checks that all the id's of the characters in the set exist in the database.
     *
     * @param characters The set containing the characters to check.
     * @return False if one of the characters does not exist, otherwise true.
     */
    private boolean charactersExist(Set<MovieCharacter> characters) {
        for (MovieCharacter character : characters) {
            if (!characterRepository.existsById(character.getId())) {
                return false;
            }
        }
        return true;
    }

}