package com.experis.no.controllers;

import com.experis.no.models.Movie;
import com.experis.no.models.MovieCharacter;
import com.experis.no.repositories.CharacterRepository;
import com.experis.no.services.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/characters")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private CharacterService characterService;

    /**
     * Method to get a character depending on the given id.
     *
     * @param id is which character you want to get.
     * @return null and a bad request if the character doesn't exist, and the character and an OK response.
     */
    @Operation(summary = "Get a character by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    public ResponseEntity<MovieCharacter> getCharacter(
            @Parameter(description = "id of character to get")
            @PathVariable(value = "id") Long id) {
        //Checks if the character exists in the database.
        if (!characterRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // Fetches the character.
        MovieCharacter character = characterRepository.getOne(id);
        HttpStatus respond = HttpStatus.OK;
        return new ResponseEntity<>(character, respond);
    }

    /**
     * Method to get all characters in the database.
     *
     * @return all characters.
     */
    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "204", description = "GET request successful, but no characters in database",
                    content = @Content)
    })
    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        List<MovieCharacter> characters = characterRepository.findAll();
        if(characters.size() == 0){
            return new ResponseEntity<>(characters, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    /**
     * Method to add a new character to the database.
     *
     * @param movieCharacter the character you want to add to the database.
     * @return returns the character and a status.
     */
    @Operation(summary = "Add a new character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Character added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid information given",
                    content = @Content)
    })
    @PostMapping()
    public ResponseEntity<MovieCharacter> addCharacter(@RequestBody MovieCharacter movieCharacter) {
        Set<Movie> movies = movieCharacter.getMovies();
        if (movies == null || !characterService.movieExists(movies)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        MovieCharacter returnCharacter = characterRepository.save(movieCharacter);
        characterService.updateCharactersInMovie(movieCharacter, movies);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Method to update a character.
     *
     * @param id             of the character you want to update
     * @param movieCharacter information you want to change in the character.
     * @return the character with updated information and a status.
     */
    @Operation(summary = "Update a character by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Character updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid information given",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content)})
    @PutMapping(value = "/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter(
            @Parameter(description = "id of character to update")
            @PathVariable(value = "id") Long id,
            @RequestBody MovieCharacter movieCharacter) {
        MovieCharacter returnCharacter = new MovieCharacter();
        HttpStatus status;
        if (!characterRepository.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        if (!id.equals(movieCharacter.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter, status);
        }
        Set<Movie> movies = movieCharacter.getMovies();
        if (movies == null) {
            movies = new HashSet<>();
        }
        if (!characterService.movieExists(movies)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        // Gets the character as they are from the database.
        MovieCharacter oldCharacter = characterRepository.getOne(id);
        // Update the movies the character appears in, and get a set with movies the database says the character
        // appears in but we have removed.
        Set<Movie> moviesToDeleteCharacterFrom = characterService.updateCharactersInMovie(movieCharacter, movies);
        // Deletes the character from the movies in the list. Mark that the oldCharacter is removed since that's the
        // character that is in the set in the database.
        characterService.deleteCharactersInMovie(oldCharacter, moviesToDeleteCharacterFrom);
        // do the update.
        returnCharacter = characterRepository.save(movieCharacter);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnCharacter, status);
    }


    /**
     * Method to delete a character.
     *
     * @param id of the character you want deleted.
     * @return the id and status.
     */
    @Operation(summary = "Delete a character by it's id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deletion successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> removeCharacter(
            @Parameter(description = "id of character to delete")
            @PathVariable(value = "id") Long id) {
        // Check that the ID exists, otherwise return a BAD_REQUEST.
        if (!characterRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        MovieCharacter characterToRemove = characterRepository.getOne(id);
        Set<Movie> movies = characterToRemove.getMovies();
        characterService.deleteCharactersInMovie(characterToRemove, movies);
        // Remove the movieCharacter and return an OK.
        characterRepository.delete(characterToRemove);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


}