package com.experis.no.services;

import com.experis.no.controllers.MovieController;
import com.experis.no.models.Movie;
import com.experis.no.models.MovieCharacter;
import com.experis.no.repositories.CharacterRepository;
import com.experis.no.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CharacterService {

    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    MovieController movieController;

    /**
     * Method to check if the movies added to characters exists.
     *
     * @param movies set of movies the characters are linked to.
     * @return false if a movie doesn't exists, true if all exists.
     */
    public boolean movieExists(Set<Movie> movies) {
        for (Movie movie : movies) {
            if (!movieRepository.existsById(movie.getId())) {
                return false;
            }
        }
        return true;
    }

    /**
     * /**
     * Method to update the characters in the movies.
     *
     * @param movieCharacter the character you want to change information about.
     * @param movies         list of movies the updated character appears in.
     * @return Set of all the movies in the database that says the character is in, but isn't.
     */
    public Set<Movie> updateCharactersInMovie(MovieCharacter movieCharacter, Set<Movie> movies) {
        //The character as they are in the database.
        MovieCharacter mc = characterRepository.getOne(movieCharacter.getId());
        //List of movies with the character according to the database.
        Set<Movie> moviesCharacterNotIn = mc.getMovies();
        for (Movie movie : movies) {
            // Gets information about the movie in the list.
            movie = movieRepository.getOne(movie.getId());
            // If the movie doesn't have any characters, create a new HashSet and adds the character to the list.
            // Then remove the movie from the list of movies according to the database and do the update.
            if (movie.getCharacters() == null) {
                Set<MovieCharacter> temp = new HashSet<>();
                temp.add(movieCharacter);
                movie.setCharacters(temp);
                moviesCharacterNotIn.remove(movie);
            }
            // Sets the temporary set equals to the characters in the movie according to the database, adds the
            // character and remove the movie from the list of movies according to the database and do the update.
            else {
                Set<MovieCharacter> temp = movie.getCharacters();
                temp.add(movieCharacter);
                movie.setCharacters(temp);
                moviesCharacterNotIn.remove(movie);
            }
            movieController.updateMovie(movie.getId(), movie);
        }
        return moviesCharacterNotIn;
    }

    /**
     * Method to delete a character from movies.
     *
     * @param movieCharacter the character to be deleted.
     * @param movies         list of movies to delete the character from.
     */
    public void deleteCharactersInMovie(MovieCharacter movieCharacter, Set<Movie> movies) {
        // Removes the character from the movies in the list.
        for (Movie movie : movies) {
            //Gets information about the movie from the database.
            movie = movieRepository.getOne(movie.getId());
            Set<MovieCharacter> temp = movie.getCharacters();
            temp.remove(movieCharacter); //Removes the character as they are in the database.
            movie.setCharacters(temp);
            movieController.updateMovie(movie.getId(), movie);
        }
    }
}
