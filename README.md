# Assignment 5 - Movie Character API

#### by Pranav Bhart, Renze Stolte, Quan Tran and Ådne Rosenvinge

## Description

This application is for the storage and manipulation of information related to movie characters, movies and franchises. We have implemented all required functionality for this assignment, even done it so that when a new character or movie is added you can also add the relevant related resources. 

## Usage
The API uses RestController from the Spring framework to handle requests. There is also a JSON-file with a collection
of Postman requests to the endpoints listed below in the postman folder in the project.
### Postman
If you don't have Postman installed, download the installer from https://www.postman.com/downloads/ or use the 
web-app https://web.postman.co/. You will need a user.

To get the Postman collection, open your Postman, and on the left side there is an import button. Click on the import 
button and drag the JSON-file in the postman folder into the window. A new window will pop up and click on import in
the bottom right. You will now have imported the API requests.

## Data
The database used in this application is PostgreSQL 13. The database is deployed on heroku.
The database is separated in the following tables with the following information:

### MovieCharacters
```
    - Id
    - Full name
    - Alias (if relevant)
    - Gender
    - Picture
```
### Movie
```
    - Id
    - Movie title
    - Genre
    - Release year
    - Director
    - Picture (for example a movie poster)
    - Trailer (for example a Youtube link)
```
### Franchise
```
    - Id
    - Name
    - Description
```

## Endpoints
All endpoint documentation can be found here: https://assignment5-moviecharacterapi.herokuapp.com/

### Characters:
```
    /characters - get all characters
    /characters - create characters
    /characters/{id} - get character by id
    /characters/{id} - delete character by id
    /characters/{id} - update character by id
```
### Movies:
```
    /movies - get all franchises
    /movies - create movie
    /movies/{id} - get movie by id
    /movies/{id} - delete movie by id
    /movies/{id} - update movie by id
    /movies/{id}/characters - get all characters from a movie
```

### Franchises:

```
    /franchises - get all franchises
    /franchises - create franchise
    /franchises/{id} - get franchise by id
    /franchises/{id} - delete franchise by id
    /franchises/{id} - update franchise by id
    /franchises/{id}/movies - get all movies from a franchise
    /franchises/{id}/characters - get all characters from a franchise
```
### Future Expansions

There are many easy ways and directions this program can be expanded. Additional API endpoints can be added simply by adding a controller, or method in an existing controller. 

If you for example want to add an endpoint that returns all the characters in a franchise sorted by occurences this can be done by copying the existing ```getCharactersByFranchise()```, and adding a sorting algorithm at the end. 

