FROM openjdk:15
ADD target/MovieCharacterApi-0.0.1-SNAPSHOT.jar moviecharacterapi.jar
ENTRYPOINT ["java", "-jar", "/moviecharacterapi.jar"]